package ee.ttu.mektory.satellite.grpcmessaging;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class MessagingClient {
    private final String HOST = "localhost";
    private final int PORT = 8888;
    private final ManagedChannel channel;
    private final MessagingServiceGrpc.MessagingServiceStub asyncStub;

    public MessagingClient() {
        channel = ManagedChannelBuilder.forAddress(HOST, PORT)
                .usePlaintext(true)
                .build();
        asyncStub = MessagingServiceGrpc.newStub(channel);
    }

    public void chat() throws InterruptedException {
        final CountDownLatch finishLatch = new CountDownLatch(1);

        StreamObserver<Message> requestObserver = asyncStub.chat(new StreamObserver<Message>() {
            @Override
            public void onNext(Message m) {
                System.out.println(String.format("Received message from server: %s", m.getText()));
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("Error");
                finishLatch.countDown();

            }

            @Override
            public void onCompleted() {
                System.out.println("Chat finished");
                finishLatch.countDown();
            }
        });

        try {
            List<Message> requests = new ArrayList<>();

            Message r = Message.newBuilder()
                    .setText("7ea8a8aaa682a8608ea49eaa9c886103f00100000045617274682063616c6c696e6799ae7e")
                    .build();
            requests.add(r);

            for (Message request : requests) {
                requestObserver.onNext(request);
                System.out.println("Sent message to server");
            }
        } catch (RuntimeException e) {
            // RPC cancel
            requestObserver.onError(e);
            throw e;
        }

        // End of requests
        requestObserver.onCompleted();

        // Asynchronous receival
        finishLatch.await(1, TimeUnit.MINUTES);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(10, TimeUnit.SECONDS);
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Building client");

        MessagingClient client = new MessagingClient();
        try {
            System.out.println("Sending messages");
            client.chat();
        } finally {
            System.out.println("Shutting down");
            client.shutdown();
        }
    }
}
