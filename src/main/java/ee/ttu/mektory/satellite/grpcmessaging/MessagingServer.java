package ee.ttu.mektory.satellite.grpcmessaging;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessagingServer implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(MessagingServer.class, args);
    }

    // Access command line arguments
    @Override
    public void run(String... args) {
        // Do something
        System.out.println("Running messaging server...");
    }
}
