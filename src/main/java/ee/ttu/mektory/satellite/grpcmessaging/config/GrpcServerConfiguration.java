package ee.ttu.mektory.satellite.grpcmessaging.config;

import ee.ttu.mektory.satellite.grpcmessaging.GrpcServerRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GrpcServerConfiguration {

    @Bean
    public GrpcServerRunner grpcServerRunner() {
        return new GrpcServerRunner();
    }
}
