package ee.ttu.mektory.satellite.grpcmessaging;

import ee.ttu.mektory.satellite.grpcmessaging.jms.UdpOutConnection;
import io.grpc.stub.StreamObserver;

import java.util.ArrayList;
import java.util.List;

public class MessagingServerImpl extends MessagingServiceGrpc.MessagingServiceImplBase {

    private UdpOutConnection udpOutConnection;

    public MessagingServerImpl(UdpOutConnection udpOutConnection) {
        this.udpOutConnection = udpOutConnection;
    }

    @Override
    public StreamObserver<Message> chat(final StreamObserver<Message> responseObserver) {
        return new StreamObserver<Message>() {
            @Override
            public void onNext(Message m) {
                String message = m.getText();

                System.out.println(String.format("Received message from client: %s", message));

                System.out.println("Sent received message to queue");
                udpOutConnection.sendAX25Frame(message);

                List<Message> responses = new ArrayList<>();

                Message r = Message.newBuilder()
                        .setText(message)
                        .build();
                responses.add(r);

                for (Message response : responses) {
                    responseObserver.onNext(response);
                    System.out.println("Sent received message to client");
                }
            }

            @Override
            public void onError(Throwable t) {
                System.out.println("Error");
            }

            @Override
            public void onCompleted() {
                System.out.println("Chat finished");
                responseObserver.onCompleted();
            }
        };
    }
}
