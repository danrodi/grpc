package ee.ttu.mektory.satellite.grpcmessaging;

import ee.ttu.mektory.satellite.grpcmessaging.jms.UdpOutConnection;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.util.Optional;

public class GrpcServerRunner implements CommandLineRunner, DisposableBean {

    @Autowired
    private UdpOutConnection udpOutConnection;

    private Server server;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Starting gRPC server...");
        server = ServerBuilder
                .forPort(8888)
                .addService(new MessagingServerImpl(udpOutConnection)).build();

        server.start();
        startDaemonAwaitThread();
        System.out.println("gRPC server started.");
    }

    private void startDaemonAwaitThread() {
        Thread awaitThread = new Thread(()->{
            try {
                GrpcServerRunner.this.server.awaitTermination();
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
        });
        awaitThread.setDaemon(false);
        awaitThread.start();
    }

    @Override
    public void destroy() {
        System.out.println("Shutting down gRPC server...");

        Optional.ofNullable(server).ifPresent(Server::shutdown);
        System.out.println("gRPC server shut down.");
    }
}
