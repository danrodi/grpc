package ee.ttu.mektory.satellite.grpcmessaging.jms;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service
public class UdpOutConnection {

    private JmsTemplate jmsTemplate;
    private Destination destination = new ActiveMQQueue("udpOut");

    public UdpOutConnection(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public void sendAX25Frame(String hex) {
        jmsTemplate.convertAndSend(destination, hex);
    }
}
