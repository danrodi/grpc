package ee.ttu.mektory.satellite.grpcmessaging.jms;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.TextMessage;

@Service
public class UdpInListener {

    @JmsListener(destination = "Consumer.messageSaver.VirtualTopic.udpIn", containerFactory = "topicListenerFactory")
    public void udpInListener(Object rawMessage) {
        TextMessage textMessage = (TextMessage) rawMessage;

        try {
            String message = textMessage.getText();

            System.out.println(String.format("Received message from queue: %s", message));
            // saveRawMessage(UUID.randomUUID().toString(), message.substring(34, message.length() - 6), BusMessageDirection.IN);
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
